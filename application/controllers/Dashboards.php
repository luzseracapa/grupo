<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboards extends CI_Controller {
	function __construct()
    {
      parent::__construct();

       $this->load->model('Dashboard');
    }
	public function index()
	{
		$data["ViewsByYear2020"]=$this->Dashboard->getViewsByYear2020();
		$data["ViewsByYear2021"]=$this->Dashboard->getViewsByYear2021();
		$data["ViewsByYear2022"]=$this->Dashboard->getViewsByYear2022();
		$data["ViewsByYear2023"]=$this->Dashboard->getViewsByYear2023();
		$data["TotalbyYear2020"]=$this->Dashboard->getTotalByYear2020();
		$data["TotalbyYear2021"]=$this->Dashboard->getTotalByYear2021();
		$data["TotalbyYear2022"]=$this->Dashboard->getTotalByYear2022();
		$data["TotalbyYear2023"]=$this->Dashboard->getTotalByYear2023();
		$data["ApprovalRate"]=$this->Dashboard->ApprovalRate();
		$data["Top5Solicitants"]=$this->Dashboard->Top5Solicitants();
		$data["NotificationsofTop5Solicitants"]=$this->Dashboard->NotificationsofTop5Solicitants();
		$data["NotificationsByParroquia"]=$this->Dashboard->NotificationsByParroquia();
		$this->load->view('header');
    $this->load->view('Dashboard/index',$data);
    $this->load->view('footer');

	}
	// public function dash2 ()
	// {
	// 	$data["ApprovalRate"]=$this->Dashboard->ApprovalRate();
	// 	$this->load->view('header');
	// 	$this->load->view('header');
	// 	$this->load->view('footer');
	// }
	// public function dash3 ()
	// {
	// 	$data["ApprovalRate"]=$this->Dashboard->ApprovalRate();
	// 	$this->load->view('header');
	// 	$this->load->view('header');
	// 	$this->load->view('footer');
	// }
	// public function dash4 ()
	// {
	// 	$data["ApprovalRate"]=$this->Dashboard->ApprovalRate();
	// 	$this->load->view('header');
	// 	$this->load->view('header');
	// 	$this->load->view('footer');
	// }

}
