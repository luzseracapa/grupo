<?php
  class Dashboard extends CI_Model
  {

    function __construct()
    {
      parent::__construct();

    }

    function getViewsByYear2020(){
      $sql="select DATE_FORMAT(fecha_con, '%Y') AS Year, MONTHNAME(fecha_con) AS Month, COUNT(*) AS Vistas
      FROM contador WHERE DATE_FORMAT(fecha_con, '%Y') = '2020' GROUP BY Year, Month ORDER BY Year, MONTH(fecha_con); ";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
          return $result->result();
      } else {
        return 0;
      }
    }
    function getViewsByYear2021(){
      $sql="select DATE_FORMAT(fecha_con, '%Y') AS Year, MONTHNAME(fecha_con) AS Month, COUNT(*) AS Vistas FROM contador WHERE DATE_FORMAT(fecha_con, '%Y') = '2021' GROUP BY Year, Month ORDER BY Year, MONTH(fecha_con); ";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
          return $result->result();
      } else {
        return 0;
      }
    }
    function getViewsByYear2022(){
      $sql="select DATE_FORMAT(fecha_con, '%Y') AS Year, MONTHNAME(fecha_con) AS Month, COUNT(*) AS Vistas FROM contador WHERE DATE_FORMAT(fecha_con, '%Y') = '2022' GROUP BY Year, Month ORDER BY Year, MONTH(fecha_con); ";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
          return $result->result();
      } else {
        return 0;
      }
    }
    function getViewsByYear2023(){
      $sql="select DATE_FORMAT(fecha_con, '%Y') AS Year, MONTHNAME(fecha_con) AS Month, COUNT(*) AS Vistas FROM contador WHERE DATE_FORMAT(fecha_con, '%Y') = '2023' GROUP BY Year, Month ORDER BY Year, MONTH(fecha_con); ";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
          return $result->result();
      } else {
        return 0;
      }
    }


    function getTotalByYear2020(){
      $sql="select DATE_FORMAT(fecha_con, '%Y') AS Year, COUNT(*) AS Views FROM contador WHERE DATE_FORMAT(fecha_con, '%Y') = '2020' GROUP BY DATE_FORMAT(fecha_con, '%Y') ORDER BY Year; ";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
          return $result->row();
      } else {
        return 0;
      }
    }
    function getTotalByYear2021(){
      $sql="select DATE_FORMAT(fecha_con, '%Y') AS Year, COUNT(*) AS Views FROM contador WHERE DATE_FORMAT(fecha_con, '%Y') = '2021' GROUP BY DATE_FORMAT(fecha_con, '%Y') ORDER BY Year; ";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
          return $result->row();
      } else {
        return 0;
      }
    }
    function getTotalByYear2022(){
      $sql="select DATE_FORMAT(fecha_con, '%Y') AS Year, COUNT(*) AS Views FROM contador WHERE DATE_FORMAT(fecha_con, '%Y') = '2022' GROUP BY DATE_FORMAT(fecha_con, '%Y') ORDER BY Year; ";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
          return $result->row();
      } else {
        return 0;
      }
    }
    function getTotalByYear2023(){
      $sql="select DATE_FORMAT(fecha_con, '%Y') AS Year, COUNT(*) AS Views FROM contador WHERE DATE_FORMAT(fecha_con, '%Y') = '2023' GROUP BY DATE_FORMAT(fecha_con, '%Y') ORDER BY Year; ";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
          return $result->row();
      } else {
        return 0;
      }
    }

    function ApprovalRate(){
      $sql="select YEAR(fecha_sol) AS year, COUNT(*) AS total_requests, SUM(CASE WHEN estado_sol = 'APROBADO' THEN 1 ELSE 0 END) AS approved_requests, (SUM(CASE WHEN estado_sol = 'APROBADO' THEN 1 ELSE 0 END) / COUNT(*)) * 100 AS approval_rate FROM solicitud_permiso GROUP BY year ORDER BY year; ";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
          return $result->result();
      } else {
        return 0;
      }
    }
    //get total productosbystock

    function Top5Solicitants(){
      $sql="select usuario.nombre_usu, usuario.email_usu, COUNT(solicitud_permiso.codigo_usu) AS solicitud_count FROM usuario JOIN solicitud_permiso ON usuario.codigo_usu = solicitud_permiso.codigo_usu GROUP BY usuario.codigo_usu, usuario.nombre_usu, usuario.email_usu ORDER BY solicitud_count DESC LIMIT 5; ";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
          return $result->result();
      } else {
        return 0;
      }
    }

    function NotificationsofTop5Solicitants(){
      $sql="select top_solicitors.email_sol, COUNT(n.codigo_not) AS notification_count FROM (SELECT email_sol FROM solicitud_permiso GROUP BY email_sol ORDER BY COUNT(codigo_sol) DESC LIMIT 5) top_solicitors LEFT JOIN notificacion n ON top_solicitors.email_sol = n.email_not GROUP BY top_solicitors.email_sol ORDER BY notification_count DESC; ";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
          return $result->result();
      } else {
        return 0;
      }
    }

    function NotificationsByParroquia(){
      $sql="select sp.parroquia_sol, COUNT(n.codigo_not) AS notification_count FROM solicitud_permiso sp LEFT JOIN notificacion n ON sp.codigo_sol = n.codigo_sol GROUP BY sp.parroquia_sol order by notification_count desc; ";
      $result=$this->db->query($sql);
      if ($result->num_rows()>0) {
          return $result->result();
      } else {
        return 0;
      }
    }
  }//close the clas

 ?>
