<br>
<h1 class="text-center">Dashboard</h1>
<div class="container-fluid pt-4 px-4">
  <div class="row g-4">
    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
        <i class="fa fa-chart-line fa-3x"></i>
        <div class="ms-3">
          <p class="mb-2 text-center">Total de vistas 2020</p>
          <?php if ($TotalbyYear2020) : ?>
            <h4 class="text-primary"><?php echo $TotalbyYear2020->Views; ?></h4>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
        <i class="fa fa-chart-line fa-3x"></i>
        <div class="ms-3">
          <p class="mb-2 text-center">Total de vistas 2021</p>
          <?php if ($TotalbyYear2021) : ?>
            <h4 style="Color:green"><?php echo $TotalbyYear2021->Views; ?></h4>
          <?php endif; ?>

        </div>
      </div>
    </div>

    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
        <i class="fa fa-chart-line fa-3x "></i>
        <div class="ms-3">
          <p class="mb-2 text-center">Todal de vistas 2022</p>
          <?php if ($TotalbyYear2022) : ?>
            <h4 style="Color:orange"><?php echo $TotalbyYear2022->Views; ?></h4>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
        <i class="fa fa-chart-line fa-3x "></i>
        <div class="ms-3">
          <p class="mb-2 text-center">Total de vista 2023</p>
          <?php if ($TotalbyYear2023) : ?>
            <h4 style="color:orange"><?php echo $TotalbyYear2023->Views; ?></h4>
          <?php endif; ?>
        </div>
      </div>
    </div>



  </div>
</div>
<div class="container-fluid pt-4 px-4">
  <div class="row g-3">
    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary text-center rounded p-4">
        <div class="d-flex align-items-center justify-content-between mb-4">
        </div>
        <canvas id="year1"></canvas>
      </div>
    </div>
    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary text-center rounded p-4">
        <div class="d-flex align-items-center justify-content-between mb-4">
        </div>
        <canvas id="year2"></canvas>
      </div>
    </div>

   

    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary text-center rounded p-4">
        <div class="d-flex align-items-center justify-content-between mb-4">
        </div>
        <canvas id="year3"></canvas>
      </div>
    </div>
    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary text-center rounded p-4">
        <div class="d-flex align-items-center justify-content-between mb-4">
        </div>
        <canvas id="year4"></canvas>
      </div>
    </div>
    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
        <i class="fa fa-chart-line fa-3x text-primary"></i>
        <div class="ms-3">
            <h2 class="text-center">DASHBOARD 5</h2>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
        <i class="fa fa-chart-line fa-3x text-primary"></i>
        <div class="ms-3">
            <h2 class="text-center">DASHBOARD 6</h2>
        </div>
      </div>
    </div>
    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
        <i class="fa fa-chart-line fa-3x text-primary"></i>
        <div class="ms-3">
            <h2 class="text-center">DASHBOARD 7</h2>
        </div>
      </div>
    </div>
    
    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary rounded d-flex align-items-center justify-content-between p-4">
        <i class="fa fa-chart-line fa-3x text-primary"></i>
        <div class="ms-3">
            <h2 class="text-center">DASHBOARD 8</h2>
        </div>
      </div>
    </div>

  </div>
</div>

<div class="container-fluid pt-4 px-4">
  <div class="row g-3">
  
  <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary text-center rounded p-4">
        <div class="d-flex align-items-center justify-content-between mb-4">
        </div>
        <h5 class="text-center text-primary">Requests Approval Rate by year (%)</h5>
        <canvas id="kpi1"></canvas>
      </div>
    </div>
    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary text-center rounded p-4">
        <div class="d-flex align-items-center justify-content-between mb-4">
        </div>
        <h5 class="text-center text-primary">Top 5 Solicitants</h5>
        <canvas id="kpi2"></canvas>
      </div>
    </div>
    <br>

    
  <div class=" col-sm-12 col-xl-3">
      <div class="bg-secondary text-center rounded p-4">
        <div class="d-flex align-items-center justify-content-between mb-4">
        </div>
        <h5 class="text-center text-primary">Request Notifications by Solicitants</h5>
        <canvas id="kpi3"></canvas>
      </div>
    </div>
    <div class="col-sm-12 col-xl-3">
      <div class="bg-secondary text-center rounded p-4">
        <div class="d-flex align-items-center justify-content-between mb-4">
        </div>
        <h5 class="text-center text-primary">Notification Activity by parroquia</h5>
        <canvas id="kpi4"></canvas>
      </div>
    </div>
  
    
    
   

    

  </div>
  <div class="row g-3">
 
  </div>
</div>
<script>
  document.addEventListener('DOMContentLoaded', function() {
    var infoButton = document.getElementById('infoButton');
    var messageContainer = document.getElementById('messageFiles');

    infoButton.addEventListener('click', function() {
      // Alternar la visibilidad del mensaje al hacer clic en el icono
      if (messageContainer.style.display === 'block') {
        hideMessage();
      } else {
        showMessage('Este es el año con menos  <strong>VISITAS</strong>.');
      }
    });

    // Agregar un evento click al documento para cerrar el mensaje al hacer clic afuera
    document.addEventListener('click', function(event) {
      if (!messageContainer.contains(event.target) && !infoButton.contains(event.target)) {
        hideMessage();
      }
    });

    function showMessage(message) {
      messageContainer.innerHTML = message;
      messageContainer.style.display = 'block';
    }

    function hideMessage() {
      messageContainer.style.display = 'none';
    }
  });
</script>

<script type="text/javascript">
  // Paleta de colores
  var colores = [
    '#a5ee8d',
    'green',
    'blue',
    'orange',
    'red',
    'purple',
    'yellow',
    'cyan',
    'pink',
    'brown'];

  // Datos del gráfico
  var datos = {
    labels: [
      <?php if ($ViewsByYear2020) : ?>
        <?php foreach ($ViewsByYear2020 as $data) : ?> '<?php echo $data->Month; ?>',
        <?php endforeach; ?>
      <?php endif; ?>
    ],
    datasets: [{
      label: 'Vistas del 2020',
      data: [
        <?php if ($ViewsByYear2020) : ?>
          <?php foreach ($ViewsByYear2020 as $data2) : ?> '<?php echo $data2->Vistas; ?>',
          <?php endforeach; ?>
        <?php endif; ?>
      ], // Valores de las barras
      backgroundColor: colores.slice(0, <?php echo count($ViewsByYear2020); ?>), // Asignar colores dinámicamente
      borderColor: [
        '#d2b5cb'
      ],
      borderWidth: 1
    }]
  };

  // Opciones de configuración
  var opciones = {
    scales: {
      y: {
        beginAtZero: true
      }
    }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('year1').getContext('2d');

  // Crear el gráfico de barras
  var graficoDeBarras = new Chart(contexto, {
    type: 'line',
    data: datos,
    options: opciones
  });
</script>

<script>
  document.addEventListener('DOMContentLoaded', function() {
    var infoButton2020 = document.getElementById('infoButton2020');
    var messageContainer2020 = document.getElementById('messageFiles2020');

    infoButton2020.addEventListener('click', function() {
      // Alternar la visibilidad del mensaje al hacer clic en el icono
      if (messageContainer2020.style.display === 'block') {
        hideMessage();
      } else {
        showMessage('Este es el año con muchas  <strong>VISITAS</strong>.');
      }
    });

    // Agregar un evento click al documento para cerrar el mensaje al hacer clic afuera
    document.addEventListener('click', function(event) {
      if (!messageContainer2020.contains(event.target) && !infoButton2020.contains(event.target)) {
        hideMessage();
      }
    });

    function showMessage(message) {
      messageContainer2020.innerHTML = message;
      messageContainer2020.style.display = 'block';
    }

    function hideMessage() {
      messageContainer2020.style.display = 'none';
    }
  });
</script>

<script type="text/javascript">
  var datos = {
    labels: [
      <?php if ($ViewsByYear2021) : ?>
        <?php foreach ($ViewsByYear2021 as $data) : ?> '<?php echo $data->Month; ?>',
        <?php endforeach; ?>
      <?php endif; ?>
    ],
    datasets: [{
      label: 'vistas del 2021',
      data: [
        <?php if ($ViewsByYear2021) : ?>
          <?php foreach ($ViewsByYear2021 as $data2) : ?> '<?php echo $data2->Vistas; ?>',
          <?php endforeach; ?>
        <?php endif; ?>
      ], // Valores de las barras
      backgroundColor: [
        '#9f42ac', // Color de la primera barra
        '#20c67a', // Color de la segunda barra
        '#6aa3b4',
        '#d6a735',
        '#9e6788',
        '#1d3d33',
        '#009ee5',
        '#5b653d',
        '#d436fd',
        '#f52557',
        '#6f7357',
        '#b93af8' // Color de la tercera barra
      ],
      borderColor: [
        '#d2b5cb'
      ],
      borderWidth: 1
    }]
  };

  // Opciones de configuraci�n
  var opciones = {
    scales: {
      y: {
        beginAtZero: true
      }
    }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('year2').getContext('2d');

  // Crear el gr�fico de barras
  var graficoDeBarras = new Chart(contexto, {
    type: 'line',
    data: datos,
    options: opciones
  });
</script>
<script>
  document.addEventListener('DOMContentLoaded', function() {
    var infoButton2022 = document.getElementById('infoButton2022');
    var messageContainer2022 = document.getElementById('messageFiles2022');

    infoButton2022.addEventListener('click', function() {
      // Alternar la visibilidad del mensaje al hacer clic en el icono
      if (messageContainer2022.style.display === 'block') {
        hideMessage();
      } else {
        showMessage('Este es el año Enenro tuvo  <strong>VISITAS</strong>.');
      }
    });

    // Agregar un evento click al documento para cerrar el mensaje al hacer clic afuera
    document.addEventListener('click', function(event) {
      if (!messageContainer2022.contains(event.target) && !infoButton2022.contains(event.target)) {
        hideMessage();
      }
    });

    function showMessage(message) {
      messageContainer2022.innerHTML = message;
      messageContainer2022.style.display = 'block';
    }

    function hideMessage() {
      messageContainer2022.style.display = 'none';
    }
  });
</script>
<script type="text/javascript">
  var datos = {
    labels: [
      <?php if ($ViewsByYear2022) : ?>
        <?php foreach ($ViewsByYear2022 as $data) : ?> '<?php echo $data->Month; ?>',
        <?php endforeach; ?>
      <?php endif; ?>
    ],
    datasets: [{
      label: 'vistas del 2022',
      data: [
        <?php if ($ViewsByYear2022) : ?>
          <?php foreach ($ViewsByYear2022 as $data2) : ?> '<?php echo $data2->Vistas; ?>',
          <?php endforeach; ?>
        <?php endif; ?>
      ], // Valores de las barras
      backgroundColor: [
        '#525671', // Color de la primera barra
        '#20c67a', // Color de la segunda barra
        '#6aa3b4',
        '#d6a735',
        '#9e6788',
        '#1d3d33',
        '#009ee5',
        '#5b653d',
        '#d436fd',
        '#f52557',
        '#6f7357',
        '#b93af8' // Color de la tercera barra
      ],
      borderColor: [
        '#d2b5cb'
      ],
      borderWidth: 1
    }]
  };

  // Opciones de configuraci�n
  var opciones = {
    scales: {
      y: {
        beginAtZero: true
      }
    }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('year3').getContext('2d');

  // Crear el gr�fico de barras
  var graficoDeBarras = new Chart(contexto, {
    type: 'line',
    data: datos,
    options: opciones
  });
</script>
<script>
  document.addEventListener('DOMContentLoaded', function() {
    var infoButton2023 = document.getElementById('infoButton2023');
    var messageContainer2023 = document.getElementById('messageFiles2023');

    infoButton2023.addEventListener('click', function() {
      // Alternar la visibilidad del mensaje al hacer clic en el icono
      if (messageContainer2023.style.display === 'block') {
        hideMessage();
      } else {
        showMessage('Este es el año diciembre tuvo menos <strong>VISITAS</strong>.');
      }
    });

    // Agregar un evento click al documento para cerrar el mensaje al hacer clic afuera
    document.addEventListener('click', function(event) {
      if (!messageContainer2023.contains(event.target) && !infoButton2023.contains(event.target)) {
        hideMessage();
      }
    });

    function showMessage(message) {
      messageContainer2023.innerHTML = message;
      messageContainer2023.style.display = 'block';
    }

    function hideMessage() {
      messageContainer2023.style.display = 'none';
    }
  });
</script>

<script type="text/javascript">
  var datos = {
    labels: [
      <?php if ($ViewsByYear2023) : ?>
        <?php foreach ($ViewsByYear2023 as $data) : ?> '<?php echo $data->Month; ?>',
        <?php endforeach; ?>
      <?php endif; ?>
    ],
    datasets: [{
      label: 'vistas del 2023',
      data: [
        <?php if ($ViewsByYear2023) : ?>
          <?php foreach ($ViewsByYear2023 as $data2) : ?> '<?php echo $data2->Vistas; ?>',
          <?php endforeach; ?>
        <?php endif; ?>
      ], // Valores de las barras
      backgroundColor: [
        '#55e4a3', // Color de la primera barra
        '#20c67a', // Color de la segunda barra
        '#6aa3b4',
        '#d6a735',
        '#9e6788',
        '#1d3d33',
        '#009ee5',
        '#5b653d',
        '#d436fd',
        '#f52557',
        '#6f7357',
        '#b93af8' // Color de la tercera barra
      ],
      borderColor: [
        '#d2b5cb'
      ],
      borderWidth: 1
    }]
  };

  // Opciones de configuraci�n
  var opciones = {
    scales: {
      y: {
        beginAtZero: true
      }
    }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('year4').getContext('2d');

  // Crear el gr�fico de barras
  var graficoDeBarras = new Chart(contexto, {
    type: 'line',
    data: datos,
    options: opciones
  });
</script>
<script>
  document.addEventListener('DOMContentLoaded', function() {
    var infoButton5 = document.getElementById('infoButton5');
    var messageContainer5 = document.getElementById('messageFiles5');

    infoButton5.addEventListener('click', function() {
      // Alternar la visibilidad del mensaje al hacer clic en el icono
      if (messageContainer5.style.display === 'block') {
        hideMessage();
      } else {
        showMessage('En el año 2020 se aprobaron mas  <strong>solicitudes</strong>.');
      }
    });

    // Agregar un evento click al documento para cerrar el mensaje al hacer clic afuera
    document.addEventListener('click', function(event) {
      if (!messageContainer5.contains(event.target) && !infoButton5.contains(event.target)) {
        hideMessage();
      }
    });

    function showMessage(message) {
      messageContainer5.innerHTML = message;
      messageContainer5.style.display = 'block';
    }

    function hideMessage() {
      messageContainer5.style.display = 'none';
    }
  });
</script>
<script type="text/javascript">
  var datos = {
    labels: [

      <?php if ($ApprovalRate) : ?>
        <?php foreach ($ApprovalRate as $data) : ?> '<?php echo $data->year; ?>',
        <?php endforeach; ?>
      <?php endif; ?>


    ],
    datasets: [{
      label: 'Requests Approval Rate by year ',
      data: [

      <?php if ($ApprovalRate) : ?>
        <?php foreach ($ApprovalRate as $data) : ?> '<?php echo $data->approval_rate; ?>',
        <?php endforeach; ?>
      <?php endif; ?>

      ], // Valores de las barras
      backgroundColor: [
        '#9e6788',
        '#1d3d33',
        '#009ee5',
        '#5b653d'
      ],
      borderColor: [
        '#9e6788',
        '#1d3d33',
        '#009ee5',
        '#5b653d'
      ],
      borderWidth: 1
    }]
  };

  // Opciones de configuraci�n
  var opciones = {
    indexAxis: 'y',
    scales: {
      x: {
        beginAtZero: true,
        min: 0,
        max: 100
      }
    }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('kpi1').getContext('2d');

  // Crear el gr�fico de barras
  var graficoDeBarras = new Chart(contexto, {
    type: 'bar',
    data: datos,
    options: opciones,

  });
</script>
<script>
  document.addEventListener('DOMContentLoaded', function() {
    var infoButton6 = document.getElementById('infoButton6');
    var messageContainer6 = document.getElementById('messageFiles6');

    infoButton6.addEventListener('click', function() {
      // Alternar la visibilidad del mensaje al hacer clic en el icono
      if (messageContainer6.style.display === 'block') {
        hideMessage();
      } else {
        showMessage('  <strong>jesica </strong>. tinee menos solicitudes aprobadas');
      }
    });

    // Agregar un evento click al documento para cerrar el mensaje al hacer clic afuera
    document.addEventListener('click', function(event) {
      if (!messageContainer6.contains(event.target) && !infoButton6.contains(event.target)) {
        hideMessage();
      }
    });

    function showMessage(message) {
      messageContainer6.innerHTML = message;
      messageContainer6.style.display = 'block';
    }

    function hideMessage() {
      messageContainer6.style.display = 'none';
    }
  });
</script>

<script type="text/javascript">
  var datos = {
    labels: [

      <?php if ($Top5Solicitants) : ?>
        <?php foreach ($Top5Solicitants as $data) : ?> '<?php echo $data->nombre_usu; ?>',
        <?php endforeach; ?>
      <?php endif; ?>


    ],
    datasets: [{
      label: 'Top 5 Solicitants',
      data: [

      <?php if ($Top5Solicitants) : ?>
        <?php foreach ($Top5Solicitants as $data) : ?> '<?php echo $data->solicitud_count; ?>',
        <?php endforeach; ?>
      <?php endif; ?>

      ], // Valores de las barras
      backgroundColor: [
        '#55e4a3', // Color de la primera barra
        '#20c67a', // Color de la segunda barra
        '#6aa3b4',
        '#d6a735' // Color de la tercera barra
      ],
      borderColor: [
        '#55e4a3', // Color de la primera barra
        '#20c67a', // Color de la segunda barra
        '#6aa3b4',
        '#d6a735'
      ],
      borderWidth: 1
    }]
  };

  // Opciones de configuraci�n
  var opciones = {
    scales: {
      y: {
        beginAtZero: true,
        min: 0,
        max: 100
      }

    }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('kpi2').getContext('2d');

  // Crear el gr�fico de barras
  var graficoDeBarras = new Chart(contexto, {
    type: 'bar',
    data: datos,
    options: opciones
  });
</script>

<script>
  document.addEventListener('DOMContentLoaded', function() {
    var infoButton7 = document.getElementById('infoButton7');
    var messageContainer7 = document.getElementById('messageFiles7');

    infoButton7.addEventListener('click', function() {
      // Alternar la visibilidad del mensaje al hacer clic en el icono
      if (messageContainer7.style.display === 'block') {
        hideMessage();
      } else {
        showMessage('el correo de gones tiene nemos  <strong>Solicitudes </strong>.');
      }
    });

    // Agregar un evento click al documento para cerrar el mensaje al hacer clic afuera
    document.addEventListener('click', function(event) {
      if (!messageContainer7.contains(event.target) && !infoButton7.contains(event.target)) {
        hideMessage();
      }
    });

    function showMessage(message) {
      messageContainer7.innerHTML = message;
      messageContainer7.style.display = 'block';
    }

    function hideMessage() {
      messageContainer7.style.display = 'none';
    }
  });
</script>

<script type="text/javascript">
  var datos = {
    labels: [

      <?php if ($NotificationsofTop5Solicitants) : ?>
        <?php foreach ($NotificationsofTop5Solicitants as $data) : ?> '<?php echo $data->email_sol; ?>',
        <?php endforeach; ?>
      <?php endif; ?>


    ],
    datasets: [{
      label: 'Notifications',
      data: [

      <?php if ($NotificationsofTop5Solicitants) : ?>
        <?php foreach ($NotificationsofTop5Solicitants as $data) : ?> '<?php echo $data->notification_count; ?>',
        <?php endforeach; ?>
      <?php endif; ?>

      ], // Valores de las barras
      backgroundColor: [
        '#55e4a3', // Color de la primera barra
        '#20c67a', // Color de la segunda barra
        '#6aa3b4',
        '#d6a735',
        '#1d3d33'
      ],
      borderColor: [
        '#d2b5cb'
      ],
      borderWidth: 1
    }]
  };

  // Opciones de configuraci�n
  var opciones = {
    scales: {
      y: {
        beginAtZero: true,
        min: 0
      }

    }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('kpi3').getContext('2d');

  // Crear el gr�fico de barras
  var graficoDeBarras = new Chart(contexto, {
    type: 'line',
    data: datos,
    options: opciones
  });
</script>
<script type="text/javascript">
  var datos = {
    labels: [

      <?php if ($NotificationsByParroquia) : ?>
        <?php foreach ($NotificationsByParroquia as $data) : ?> '<?php echo $data->parroquia_sol; ?>',
        <?php endforeach; ?>
      <?php endif; ?>


    ],
    datasets: [{
      label: 'Notifications',
      data: [

      <?php if ($NotificationsByParroquia) : ?>
        <?php foreach ($NotificationsByParroquia as $data) : ?> '<?php echo $data->notification_count; ?>',
        <?php endforeach; ?>
      <?php endif; ?>

      ], // Valores de las barras
      backgroundColor: [
        'rgba(255, 99, 132, 0.6)', // Color de la primera barra
        'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
        'rgba(255, 206, 86, 0.6)' // Color de la tercera barra
      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)'
      ],
      borderWidth: 1
    }]
  };

  // Opciones de configuraci�n
  var opciones = {
    scales: {
      y: {
        beginAtZero: true,
        min: 0
      }

    }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('kpi4').getContext('2d');

  // Crear el gr�fico de barras
  var graficoDeBarras = new Chart(contexto, {
    type: 'pie',
    data: datos,
    options: opciones
  });
</script>
<script type="text/javascript">
  var datos = {
    labels: [

      <?php if ($NotificationsByParroquia) : ?>
        <?php foreach ($NotificationsByParroquia as $data) : ?> '<?php echo $data->parroquia_sol; ?>',
        <?php endforeach; ?>
      <?php endif; ?>


    ],
    datasets: [{
      label: 'Notifications',
      data: [

      <?php if ($NotificationsByParroquia) : ?>
        <?php foreach ($NotificationsByParroquia as $data) : ?> '<?php echo $data->notification_count; ?>',
        <?php endforeach; ?>
      <?php endif; ?>

      ], // Valores de las barras
      backgroundColor: [
        'rgba(255, 99, 132, 0.6)', // Color de la primera barra
        'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
        'rgba(255, 206, 86, 0.6)' // Color de la tercera barra
      ],
      borderColor: [
        'rgba(255, 99, 132, 1)',
        'rgba(54, 162, 235, 1)',
        'rgba(255, 206, 86, 1)'
      ],
      borderWidth: 1
    }]
  };

  // Opciones de configuraci�n
  var opciones = {
    scales: {
      y: {
        beginAtZero: true,
        min: 0
      }

    }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('kpi4').getContext('2d');

  // Crear el gr�fico de barras
  var graficoDeBarras = new Chart(contexto, {
    type: 'pie',
    data: datos,
    options: opciones
  });
</script>
